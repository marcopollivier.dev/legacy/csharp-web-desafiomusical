﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesafioMusical.Models
{
    public class Item
    {
        private int CodItem { get; set; }
        private string TipoItem { get; set; }
        private string Titulo { get; set; }
        private string Descricao { get; set; }
        private string Autor { get; set; }
        private string ano { get; set; }
    }
}