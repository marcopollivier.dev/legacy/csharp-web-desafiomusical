﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DesafioMusical.Startup))]
namespace DesafioMusical
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
